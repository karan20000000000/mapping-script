function onKeyUp(player,key)
{
  if(slot[player.ID].IsBeingEdited==true)
  {

    if(slot[player.ID].IsPos==true)
    {
      if(IsWasdUser(player.Name)==true)
      {
        switch(key)
        {
          case d: slot[player.ID].IncXT.Delete(); slot[player.ID].IncXT = null; break;
          case a: slot[player.ID].DecXT.Delete(); slot[player.ID].DecXT = null; break;
          case w: slot[player.ID].IncYT.Delete(); slot[player.ID].IncYT = null; break;
          case s: slot[player.ID].DecYT.Delete(); slot[player.ID].DecYT = null; break;
        }
      }
      else {
      switch(key)
      {
        case right: slot[player.ID].IncXT.Delete(); slot[player.ID].IncXT = null; break;
        case left: slot[player.ID].DecXT.Delete(); slot[player.ID].DecXT = null; break;
        case up: slot[player.ID].IncYT.Delete(); slot[player.ID].IncYT = null; break;
        case down: slot[player.ID].DecYT.Delete(); slot[player.ID].DecYT = null; break;
      } }
    }
    else if(slot[player.ID].IsHt==true)
    {

        if(IsWasdUser(player.Name)==true)
        {
          switch(key)
          {
            case w: slot[player.ID].IncHT.Delete(); slot[player.ID].IncHT = null; break;
            case s: slot[player.ID].DecHT.Delete(); slot[player.ID].DecHT = null; break;
          }
        }
        else {
      switch(key)
      {
        case up: slot[player.ID].IncHT.Delete(); slot[player.ID].IncHT = null; break;
        case down: slot[player.ID].DecHT.Delete(); slot[player.ID].DecHT = null; break;
      } }
    }
    else if(slot[player.ID].IsAngle==true)
    {
      if(IsWasdUser(player.Name)==true)
      {
        switch(key)
        {
          case w: slot[player.ID].IncYAT.Delete(); slot[player.ID].IncYAT = null; break;
          case s: slot[player.ID].DecYAT.Delete(); slot[player.ID].DecYAT = null; break;
          case d: slot[player.ID].IncXAT.Delete(); slot[player.ID].IncXAT = null; break;
          case a: slot[player.ID].DecXAT.Delete(); slot[player.ID].DecXAT = null; break;

          case lr: slot[player.ID].IncZAT.Delete(); slot[player.ID].IncZAT = null; break;
          case rr: slot[player.ID].DecZAT.Delete(); slot[player.ID].DecZAT = null; break;
        }
      }
      else {
      switch(key)
      {
        case up: slot[player.ID].IncYAT.Delete(); slot[player.ID].IncYAT = null; break;
        case down: slot[player.ID].DecYAT.Delete(); slot[player.ID].DecYAT = null; break;
        case right: slot[player.ID].IncXAT.Delete(); slot[player.ID].IncXAT = null; break;
        case left: slot[player.ID].DecXAT.Delete(); slot[player.ID].DecXAT = null; break;

        case lr: slot[player.ID].IncZAT.Delete(); slot[player.ID].IncZAT = null; break;
        case rr: slot[player.ID].DecZAT.Delete(); slot[player.ID].DecZAT = null; break;
      } }
  }
 }
}

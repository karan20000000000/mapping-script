function onKeyDown(player,key)
{



    if(key==f1)
    {
      if(slot[player.ID].IsBeingEdited == false) slot[player.ID].IsBeingEdited = true;
      slot[player.ID].IsPos = true; slot[player.ID].IsHt = false; slot[player.ID].IsAngle = false;
      Message(player.Name+": [#FFFFFF]Editing x,y coords now");
    }
    else if(key==f2)
    {
      if(slot[player.ID].IsBeingEdited == false) slot[player.ID].IsBeingEdited = true;
      slot[player.ID].IsPos = false; slot[player.ID].IsHt = true; slot[player.ID].IsAngle = false;
      Message(player.Name+": [#FFFFFF]Editing z coord now");
    }
    else if(key==f3)
    {
      if(slot[player.ID].IsBeingEdited == false) slot[player.ID].IsBeingEdited = true;
      slot[player.ID].IsPos = false; slot[player.ID].IsHt = false; slot[player.ID].IsAngle = true;
      Message(player.Name+": [#FFFFFF]Editing object's angles now");
    }
    else if(key==rel)
    {
      if(slot[player.ID].IsRP==false)
      {
        slot[player.ID].IsRP=true;
        Message(player.Name+": [#FFFFFF]Relative Positioning now turned: [#FF6A6A]on")
      }
      else if(slot[player.ID].IsRP==true)
      {
        slot[player.ID].IsRP=false;
        Message(player.Name+": [#FFFFFF]Relative Positioning now turned: [#FF6A6A]off")
      }
    }
    else if(key==con)
    {
      if(slot[player.ID].IsBeingEdited == false)
      { slot[player.ID].IsBeingEdited = true; Message(player.Name+": [#FFFFFF]Editing enabled"); }
      else if(slot[player.ID].IsBeingEdited == true)
      { slot[player.ID].IsBeingEdited = false; Message(player.Name+": [#FFFFFF]Editing disabled"); }
    }
    else if(key==del)
    {
      if(slot[player.ID].IsBeingEdited==false) Message(player.Name+": [#FFFFFF]First turn on editing");
      else {
      FindObject(slot[player.ID].id).Delete();
      slot[player.ID].ResetData();
      Message(player.Name+": [#FFFFFF]Object deleted"); }
    }
    else if(key==p)
    {
      if(slot[player.ID].IsBeingEdited==false) Message(player.Name+": [#FFFFFF]First turn on editing");
      else {
      local o = FindObject(slot[player.ID].id);
      print("CreateObject("+o.Model+","+o.World+",Vector"+o.Pos+","+o.Alpha+").RotateToEuler(Vector"+o.RotationEuler+",1);");
      WriteIniString("objects.ini","Objects",slot[player.ID].id.tostring(), "CreateObject("+o.Model+","+o.World+",Vector"+o.Pos+","+o.Alpha+").RotateToEuler(Vector"+o.RotationEuler+",1);");
      Message(player.Name+": [#FFFFFF]Details printed"); }
    }
    else if(key==l)
    {
      if(slot[player.ID].IsBeingEdited==false) Message(player.Name+": [#FFFFFF]First turn on editing");
      else {
      local o = CreateObject(slot[player.ID].model,player.World,Vector(player.Pos.x+2,player.Pos.y,player.Pos.z),255);
      o.TrackingShots = true;
      slot[player.ID].id = o.ID;
      Message(player.Name+": [#FFFFFF]The previous object was replicated and has been selected for editing.");
      Message(player.Name+": "+slot[player.ID].AppMsg());  }
    }
    else if(key==ins)
    {
        ClearTimers(player);

        Message(player.Name+": [#FFFFFF]Fixed the bugged object");
    }

     if(slot[player.ID].IsBeingEdited==true)
     { {
    if(slot[player.ID].IsPos==true)
    {
      if(IsWasdUser(player.Name)==true)
      {
        switch(key)
        {
          case d: slot[player.ID].IncXT = momo("slot["+player.ID+"].IncX();"); break;
          case a: slot[player.ID].DecXT = momo("slot["+player.ID+"].DecX();"); break;
          case w: slot[player.ID].IncYT = momo("slot["+player.ID+"].IncY();"); break;
          case s: slot[player.ID].DecYT = momo("slot["+player.ID+"].DecY();"); break;
        }
      }
      else {
      switch(key)
      {
        case right: slot[player.ID].IncXT = momo("slot["+player.ID+"].IncX();"); break;
        case left: slot[player.ID].DecXT = momo("slot["+player.ID+"].DecX();"); break;
        case up: slot[player.ID].IncYT = momo("slot["+player.ID+"].IncY();"); break;
        case down: slot[player.ID].DecYT = momo("slot["+player.ID+"].DecY();"); break;
      } }
    }
    else if(slot[player.ID].IsHt==true)
    {
      if(IsWasdUser(player.Name)==true)
      {
        switch(key)
        {
          case w: slot[player.ID].IncHT = momo("slot["+player.ID+"].IncH();"); break;
          case s: slot[player.ID].DecHT = momo("slot["+player.ID+"].DecH();"); break;
        }
      }
        else {
      switch(key)
      {
        case up: slot[player.ID].IncHT = momo("slot["+player.ID+"].IncH();"); break;
        case down: slot[player.ID].DecHT = momo("slot["+player.ID+"].DecH();"); break;
      }  }
    }
    else if(slot[player.ID].IsAngle==true)
    {
      if(IsWasdUser(player.Name)==true)
      {
        switch(key)
        {
          case w: slot[player.ID].IncYAT = momo("slot["+player.ID+"].IncYA();"); break;
          case s: slot[player.ID].DecYAT = momo("slot["+player.ID+"].DecYA();"); break;
          case d: slot[player.ID].IncXAT = momo("slot["+player.ID+"].IncXA();"); break;
          case a: slot[player.ID].DecXAT = momo("slot["+player.ID+"].DecXA();"); break;

          case lr: slot[player.ID].IncZAT = momo("slot["+player.ID+"].IncZA();"); break;
          case rr: slot[player.ID].DecZAT = momo("slot["+player.ID+"].DecZA();"); break;
        }
      }
      else {
      switch(key)
      {
        case up: slot[player.ID].IncYAT = momo("slot["+player.ID+"].IncYA();"); break;
        case down: slot[player.ID].DecYAT = momo("slot["+player.ID+"].DecYA();"); break;
        case right: slot[player.ID].IncXAT = momo("slot["+player.ID+"].IncXA();"); break;
        case left: slot[player.ID].DecXAT = momo("slot["+player.ID+"].DecXA();"); break;

        case lr: slot[player.ID].IncZAT = momo("slot["+player.ID+"].IncZA();"); break;
        case rr: slot[player.ID].DecZAT = momo("slot["+player.ID+"].DecZA();"); break;
      } }
    }
    }
  }
}

function momo(str)
{
  return NewTimer("comp",100,0,str);
}

function ClearTimers(player)
{  try {
   slot[player.ID].IncXT.Delete(); slot[player.ID].IncXT = null;
   slot[player.ID].DecXT.Delete(); slot[player.ID].DecXT = null;
   slot[player.ID].IncYT.Delete(); slot[player.ID].IncYT = null;
   slot[player.ID].DecYT.Delete(); slot[player.ID].DecYT = null;
   slot[player.ID].IncHT.Delete(); slot[player.ID].IncHT = null;
   slot[player.ID].DecHT.Delete(); slot[player.ID].DecHT = null;
   slot[player.ID].IncYAT.Delete(); slot[player.ID].IncYAT = null;
   slot[player.ID].DecYAT.Delete(); slot[player.ID].DecYAT = null;
   slot[player.ID].IncXAT.Delete(); slot[player.ID].IncXAT = null;
   slot[player.ID].DecXAT.Delete(); slot[player.ID].DecXAT = null;

   slot[player.ID].IncZAT.Delete(); slot[player.ID].IncZAT = null;
   slot[player.ID].DecZAT.Delete(); slot[player.ID].DecZAT = null; } catch(e) {}
}

function gfp( pos, rad, dist = 25.0  )
{

                local x = pos.x, y = pos.y;
		local x2 = x + 1.0 * cos(rad) - dist * sin(rad); // we calculate
		local y2 = y + 1.0 * sin(rad) + dist * cos(rad);
		local x3 = ((1*x2)+(99*pos.x))/100;
		local y3 = ((1*y2)+(99*pos.y))/100;
                return Vector( x3, y3, pos.z ); // return a vector
}

function AddDegreesAngle(player,deg)
{
	local rad = (PI/180*deg);

	return player.Angle + (PI/180*deg);
}
function wm(t) return Message("[#FFFFFF]"+t);
function comp(str)
{
  //try
  //{
    local a = compilestring(str);
    a();
  /**}
  catch(e) print("Error: "+e); */
}

function IsWasdUser(name)
{
	if(wasdUsers.find(name.tolower())!=null) {

 return true;
 }

	return false;
}


function LoadAllObjects()
{ local i = 0;
  while(true)
  {
    local str = ReadIniString( "objects.ini", "objects", i.tostring() );
    if(!str) break;
    else
    {
      local a = compilestring(str);
      a();

      i++;
    }
  }
  for(local a=0;a<GetObjectCount();a++)
  {
      FindObject(a).TrackingShots = true;
  }
}

function SaveObjects()
{ local n = GetObjectCount();
  for(local i=0;i<n;i++)
  {
    local o = FindObject(i);
    if(o) {
    WriteIniString("objects.ini","autosaved",i.tostring(), "CreateObject("+o.Model+","+o.World+",Vector"+o.Pos+","+o.Alpha+").RotateToEuler(Vector"+o.RotationEuler+",1);"); }
  }
}

function LoadAutoSaved()
{
  local i = 0;
    while(true)
    {
      local str = ReadIniString( "objects.ini", "autosaved", i.tostring() );
      if(!str) break;
      else
      {
        local a = compilestring(str);
        a();

        i++;
      }
    }
    for(local a=0;a<GetObjectCount();a++)
    {
        FindObject(a).TrackingShots = true;
    }
}

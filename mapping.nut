wasdUsers <- [];

class OBJ
{
  id = null;
  p = null;
  model = null;
  IsBeingEdited = false;
  IsPos = true;
  IsHt = false;
  IsAngle = false;
  IsRP = true;

  IncXT = null;
  DecXT = null;
  IncYT = null;
  DecYT = null;

  IncHT = null;
  DecHT = null;

  IncXAT = null;
  DecXAT = null;
  IncYAT = null;
  DecYAT = null;
  IncZAT = null;
  DecZAT = null;

  function ResetData()
  {
      id = null;
      model = null;
      IsBeingEdited = false;

  }



  function IncX()
  {
      if(IsRP==true)
    {
      local plr =  ::FindPlayer(p);
      local ob = ::FindObject(id);
      ob.Pos = ::gfp(ob.Pos,plr.Angle+(-1.57079633));

    }
    else
    { ::FindObject(id).Pos.x+=0.250; }

  }
  function DecX()
  {  if(IsRP==true)
    {
      local plr =  ::FindPlayer(p);
      local ob = ::FindObject(id);
      ob.Pos = ::gfp(ob.Pos,plr.Angle+(1.57079633));

    }
	else {
     ::FindObject(id).Pos.x-=0.250; }
  }
  function IncY()
  {  if(IsRP==true)
    {
      local plr =  ::FindPlayer(p);
      local ob = ::FindObject(id);
      ob.Pos = ::gfp(ob.Pos,plr.Angle);

    }
    else
     ::FindObject(id).Pos.y+=0.250;
  }
  function DecY()
  {  if(IsRP==true)
    {
      local plr =  ::FindPlayer(p);
      local ob = ::FindObject(id);
      ob.Pos = ::gfp(ob.Pos,AddDegreesAngle(plr,180));
	  //ob.Pos = ::Vector(-1*p.x,-1*p.y,p.z);
    }
	else
     ::FindObject(id).Pos.y-=0.250;
  }


  function IncH()
  {
     ::FindObject(id).Pos.z+=0.250;
  }
  function DecH()
  {
     ::FindObject(id).Pos.z-=0.250;
  }


  function IncXA()
  {
      local plr = ::FindPlayer(p);
      local obje = ::FindObject(id);
      obje.RotateByEuler(Vector(0,0,-1*plr.Angle),1);
      obje.RotateByEuler(Vector(0.050,0,0),1);
      obje.RotateByEuler(Vector(0,0,plr.Angle),1);
  }
  function DecXA()
  {
    local plr = ::FindPlayer(p);
    local obje = ::FindObject(id);
    obje.RotateByEuler(Vector(0,0,-1*plr.Angle),1);
     ::FindObject(id).RotateByEuler(Vector(-0.050,0,0),100);
     obje.RotateByEuler(Vector(0,0,plr.Angle),1);
  }
  function IncYA()
  {
    local plr = ::FindPlayer(p);
    local obje = ::FindObject(id);
    obje.RotateByEuler(Vector(0,0,-1*plr.Angle),1);
     obje.RotateByEuler(Vector(0,-0.050,0),100);
     obje.RotateByEuler(Vector(0,0,plr.Angle),1);
  }
  function DecYA()
  {
    local plr = ::FindPlayer(p);
    local obje = ::FindObject(id);
    obje.RotateByEuler(Vector(0,0,-1*plr.Angle),1);
     ::FindObject(id).RotateByEuler(Vector(0,0.050,0),100);
     obje.RotateByEuler(Vector(0,0,plr.Angle),1);
  }
  function IncZA()
  {
     ::FindObject(id).RotateByEuler(Vector(0,0,0.050),100);
  }
  function DecZA()
  {
     ::FindObject(id).RotateByEuler(Vector(0,0,-0.050),100);
  }

  function AppMsg()
  {
    if(IsPos==true)
    return "[#FFFFFF]Editing x,y coords now";
    else if(IsHt==true)
    return "[#FFFFFF]Editing z coord now";
    else if(IsAngle==true)
    return "[#FFFFFF]Editing object's angles now";
  }


}


function onPlayerJoin(player)
{
  slot[player.ID] = OBJ();
  slot[player.ID].p = player.ID;
}

function onPlayerCommand(player,cmd,text)
{
  if(cmd=="co")
  {
      if(!text) MessagePlayer("[#FFFFFF]gib id",player);
      else
      {
          try {
            slot[player.ID].model = text.tointeger();
            local ob = CreateObject(slot[player.ID].model,player.World,Vector(player.Pos.x+2,player.Pos.y,player.Pos.z),255);
            ob.TrackingShots = true;
            slot[player.ID].id = ob.ID;
            slot[player.ID].IsBeingEdited = true;
            Message(player.Name+": "+slot[player.ID].AppMsg());
            } catch(e) print(e);
      }
  }
  else if(cmd == "wasd")
  {
	if(IsWasdUser(player.Name)==true) MessagePlayer("[#FFFFFF]You're already wasd user",player);
	else
	{
		wasdUsers.push(player.Name.tolower());
		MessagePlayer("[#FFFFFF]You're now wasd user",player);
	}
  }
  else if(cmd == "arrows")
  {
	   if(IsWasdUser(player.Name)==false) MessagePlayer("[#FFFFFF]You already use arrow keys for editing",player);
	    else
	     {
		  local idx = wasdUsers.find(player.Name.tolower());
		wasdUsers.remove(idx);
		MessagePlayer("[#FFFFFF]You're no longer wasd user. You're now using arrows keys for editing",player);
	 }

  }

  else if(cmd=="loadautosaved")
  {
    LoadAutoSaved();
  }

  else if(cmd== "exe")
    {
        if(text)
        {
            try{
              local a = compilestring(text);
              a();
            }
            catch(e) { Message("[#FFFFFF]Error: "+e); }
        }
    }

}


function onPlayerSpawn(player)
{
  player.GiveWeapon(GetWeaponID("m4"),9999);
}

function onObjectShot(object,player,weapon)
{
  slot[player.ID].ResetData();
  slot[player.ID].id = object.ID;
  slot[player.ID].model = object.Model;
  slot[player.ID].IsBeingEdited = true;
  Message(player.Name+": [#FFFFFF]Object ID:["+object.ID+"], Model: ["+object.Model+"] has been selected and is ready for editing");
  Message(player.Name+": "+slot[player.ID].AppMsg());
}
